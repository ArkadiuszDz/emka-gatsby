import React from 'react';
import styled from 'styled-components';
import BackgroundImage from 'gatsby-background-image';

const Hero = ({img,children,className}) => {
  return (
    <BackgroundImage preserveStackingContext={true} className={className}  fluid={img}>
      {children}
    </BackgroundImage>
  );
}

export default styled(Hero)`
  min-height: calc(100vh - 172px);
  background: linear-gradient(rgba(63,208,212,0.7),rgba(0,0,0,0.7));
  background-position: center;
  background-size: cover;
  display: flex;
  justify-content: center;
  align-items: center;
`;