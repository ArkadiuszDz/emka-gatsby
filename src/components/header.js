import { Link } from "gatsby";
import PropTypes from "prop-types";
import React from "react";
import logo from '../images/emka.jpg';
import "../scss/header.scss";
import Navbar from './Navbar';

const Header = ({ siteTitle }) => (
  <header>
    <div className="container">
      <div className="header-content">
        <div className="logo-wrapper">
          <img src={logo} alt="logo emka" />
        </div>
        <Navbar/>
        <div className="social">
          facebook
        </div>
      </div>
    </div>
  </header>
)

Header.propTypes = {
  siteTitle: PropTypes.string,
}

Header.defaultProps = {
  siteTitle: ``,
}

export default Header;
