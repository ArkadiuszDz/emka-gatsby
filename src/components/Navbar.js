import React from 'react';
import { Link } from 'gatsby';

const Navbar = () => {
  return (
    <nav>
      <Link to="/">Strong Główna</Link>
      <Link to="/oferta">Oferta</Link>
      <Link to="/kontakt">Kontakt</Link>
    </nav>
  )
}

export default Navbar;