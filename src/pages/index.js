import React from "react";
import { Link, graphql } from "gatsby";
import Layout from "../components/Layout";
import SEO from "../components/SEO";
import Banner from "../components/Banner";
import Hero from "../components/Hero";
import "../scss/elements.scss";

const IndexPage = ({data}) => {
  console.log(data);
  return (
    <Layout>
      <SEO title="Home" />
      <Hero img={data.file.childImageSharp.fluid}>
        <Banner title="Emka - szkoła, która inspiruje" info="Zapraszamy najmłodszych na zajęcia.">
          <Link to="/oferta" className="btn btn-hollow--white">Zobacz</Link>
        </Banner>
     </Hero>
    </Layout>
  )
}

export const query = graphql`
  query {
    file(relativePath: {eq: "teach.jpg"}){
      childImageSharp{
        fluid(maxWidth: 4160, quality: 90){
          ...GatsbyImageSharpFluid_withWebp
        }
      }
    }
  }`;

export default IndexPage;
